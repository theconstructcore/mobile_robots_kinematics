{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"jumbotron m-0\">\n",
    "    <hr />\n",
    "    <h1 class=\"text-center\">\n",
    "        <span class=\"text-primary\">Basic Kinematics of Mobile Robots</span>\n",
    "    </h1>\n",
    "    <hr />\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div>\n",
    "    <h2 class=\"text-center\">\n",
    "        <span class=\"text-primary\">Unit 3:</span>\n",
    "        &nbsp;\n",
    "        <span class=\"\">Kinematics for Holonomic Robots</span>\n",
    "    </h2>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"bg-primary text-center\">\n",
    "    - Summary -\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Estimated time to completion: **3 hours**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this Unit, you will learn about the kinematics of holonomic robots. Specifically, you will learn about:\n",
    "\n",
    "* Kinematic model of a holonomic robot\n",
    "* Motion with respect to the robot's frame\n",
    "* Motion with respect to the world's frame\n",
    "\n",
    "Bibliography:\n",
    "\n",
    "Lynch, K. M., & Park, F. C. (2017). *Modern Robotics*, chapter 13. Cambridge University Press."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"bg-primary text-center\">\n",
    "    - End of Summary -\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For learning the concepts in this unit, we are going to use a simulated 3D world with a [Neobotix MPO-500 robot](https://robots.ros.org/neobotix-mpo-500/) platform."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div>\n",
    "    <h3 class=\"text-center\">\n",
    "        <span class=\"text-primary\">3.1</span>\n",
    "        &nbsp;\n",
    "        <span class=\"\">Modeling</span>\n",
    "    </h3>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A kinematic model of a mobile robot governs how wheel speeds map to robot velocities. We assume that the robot rolls on hard, flat, horizontal ground without skidding, and it has a single rigid-body chassis (not articulated like a tractor-trailer) with a configuration $T_{sb} \\in SE(2)$ representing a chassis-fixed frame {b} relative to a fixed space frame {s} in the horizontal plane.\n",
    "\n",
    "<img src=\"https://www.neobotix-robots.com/fileadmin/_processed_/4/f/csm_MPO-500_Header_834e8a73ab.jpg\" alt=\"Neobotix MPO-500 image\" width=\"300\" align=\"right\">\n",
    "\n",
    "We represent $T_{sb}$ by the three coordinates $q = (\\phi, x, y)$, and the velocity of\n",
    "the chassis as the time derivative of the coordinates, $\\dot{q} = (\\dot{\\phi}, \\dot{x}, \\dot{y})$. We also define the chassis' planar twist $\\nu_b = (\\omega_{bz}, v_{bx}, v_{by})$ expressed in {b}, where\n",
    "\n",
    "\\begin{equation}\n",
    "\\nu_b = \\begin{bmatrix}\\omega_{bz} \\\\ v_{bx} \\\\ v_{by}\\end{bmatrix} = \n",
    "\\begin{bmatrix}1 & 0 & 0 \\\\ 0 & \\phantom{-}\\cos\\phi & \\sin\\phi \\\\ 0 & -\\sin\\phi & \\cos\\phi \\end{bmatrix}\n",
    "\\begin{bmatrix}\\dot{\\phi} \\\\ \\dot{x} \\\\ \\dot{y}\\end{bmatrix}\n",
    "\\end{equation}\n",
    "\n",
    "Wheeled mobile robots may be classified in two major categories, [holonomic or (omnidirectional)](https://en.wikipedia.org/wiki/Holonomic_(robotics)) and [nonholonomic](https://en.wikipedia.org/wiki/Nonholonomic_system). Whether a wheeled mobile robot is omnidirectional or not depends in part on the type of wheels it employs. Nonholonomic mobile robots, such as conventional cars, employ conventional wheels, which prevents cars from moving directly sideways.\n",
    "\n",
    "<img src=\"images/mechanum_wheel.png\" width=\"300\" align=\"right\">\n",
    "\n",
    "Omnidirectional wheeled mobile robots typically employ either [omniwheels](https://en.wikipedia.org/wiki/Omni_wheel)\n",
    "or [mecanum wheels](https://en.wikipedia.org/wiki/Mecanum_wheel), which are typical wheels augmented with rollers\n",
    "on their outer circumference. These rollers spin freely and they allow\n",
    "sideways sliding while the wheel drives forward or backward without slip in\n",
    "that direction.\n",
    "\n",
    "Unlike the front wheels in a car, those wheels are not steered, only driven forward or backward. The [Neobotix MPO-500 robot](https://robots.ros.org/neobotix-mpo-500/) is such an holonomic robot thanks to its four omnidirectional mechanum wheels."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The most important question in kinematic modeling is:\n",
    "\n",
    "* Given a desired chassis velocity $\\dot{q}$ or twist $\\nu_b$, at what speeds must the wheels be driven?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Kinematic model\n",
    "<img src=\"images/kinemaric_model_4_wheel.png\" width=\"360\" align=\"right\">\n",
    "\n",
    "The kinematic model of the mobile robot with four mecanum wheels is:\n",
    "\n",
    "\\begin{equation}\n",
    "u = \\begin{bmatrix} u_1 \\\\ u_2 \\\\ u_3 \\\\ u_4 \\end{bmatrix} = \\frac{1}{r} \\begin{bmatrix} -l-w & 1 & -1 \\\\ \\phantom{-}l+w & 1 & \\phantom{-}1 \\\\ \\phantom{-}l+w & 1 & -1 \\\\ -l-w & 1 & \\phantom{-}1 \\end{bmatrix} \\begin{bmatrix}\\omega_{bz} \\\\ v_{bx} \\\\ v_{by} \\end{bmatrix}\n",
    "\\end{equation}\n",
    "\n",
    "where\n",
    "* $u$ is the vector containing each wheel driving speed,\n",
    "* $r$ is the radius of the wheels,\n",
    "* $l$ and $w$ are the dimensions of the chassis,\n",
    "* $\\gamma$ (in the figure) is the angle at which free \"sliding\" occurs\n",
    "\n",
    "(see \\[Lynch & Park, 2017\\] for a complete derivation of this model).\n",
    "\n",
    "For this robot, to move in the direction $\\mathrm{+x̂_b}$, all wheels drive forward\n",
    "at the same speed; to move in the direction $\\mathrm{+ŷ_b}$, wheels 1 and 3 drive backward\n",
    "and wheels 2 and 4 drive forward at the same speed; and to rotate in the\n",
    "counterclockwise direction, wheels 1 and 4 drive backward and wheels 2 and 3\n",
    "drive forward at the same speed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div>\n",
    "    <h3 class=\"text-center\">\n",
    "        <span class=\"text-primary\">3.2</span>\n",
    "        &nbsp;\n",
    "        <span class=\"\">Basic Motions</span>\n",
    "    </h3>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before sending any value, we must first run some **initialization code** for:\n",
    "* importing some modules, classes, and functions\n",
    "* initializing this notebook as a ROS node\n",
    "* creating a ROS publisher for the `wheel_speed` topic\n",
    "\n",
    "Create a new Python file in the *catkin_ws/src* directory, named *basic_motions.py*. Then add the following code on it:\n",
    " "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span class=\"badge badge-pill badge-primary\">\n",
    "    <i class=\"fa fa-play\"></i>\n",
    "    &nbsp;\n",
    "    Add this code to *basic_motions.py*\n",
    "</span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import rospy, math, numpy as np\n",
    "from std_msgs.msg import Float32MultiArray\n",
    "from utilities import reset_world\n",
    "\n",
    "rospy.init_node('holonomic_controller', anonymous=True)\n",
    "pub = rospy.Publisher('wheel_speed', Float32MultiArray, queue_size=10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"images/mpo_500_frame.png\" align=\"right\" width=\"300\">\n",
    "\n",
    "As explained in the above model, these are the **basic motions**:\n",
    "* **Forward** (move in the direction $\\mathrm{+x̂_b}$): \n",
    "all wheels drive forward at the same speed with positive values.\n",
    "\n",
    "* **Backward** (move in the direction $\\mathrm{-x̂_b}$): \n",
    "all wheels drive forward at the same speed with negative values.\n",
    "\n",
    "* **Left translation** (move in the direction $\\mathrm{+ŷ_b}$): wheels 1 and 3 drive backward\n",
    "and wheels 2 and 4 drive forward at the same speed.\n",
    "\n",
    "* **Right translation** (move in the direction $\\mathrm{-ŷ_b}$): wheels 1 and 3 drive forward\n",
    "and wheels 2 and 4 drive backward at the same speed.\n",
    "\n",
    "* **Counterclockwise rotation** (positive angle around the Z-axis): wheels 1 and 4 drive backward and wheels 2 and 3 drive forward at the same speed.\n",
    "\n",
    "* **Clockwise rotation** (negative angle around the Z-axis): wheels 1 and 4 drive forward and wheels 2 and 3 drive backward at the same speed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"images/holo_forward.gif\" align=\"right\">\n",
    "Let's start by moving the robot forward (+X) and stopping after 5 seconds.\n",
    "\n",
    "The [`Float32MultiArray`](http://docs.ros.org/melodic/api/std_msgs/html/msg/Float32MultiArray.html) ROS structure is the equivalent of an array in Python or any other programming language. We set the values of the array with the speed for each of the four wheels of the robot, and publish the message to the topic.\n",
    "\n",
    "After a 5-seconds pause with the function `rospy.sleep`, we send another message with all the velocities set to zero for stopping the robot."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span class=\"badge badge-pill badge-primary\">\n",
    "    <i class=\"fa fa-play\"></i>\n",
    "    &nbsp;\n",
    "    Add this code to *basic_motions.py* \n",
    "</span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "forward = [1, 1, 1, 1]\n",
    "msg = Float32MultiArray(data=forward)\n",
    "pub.publish(msg)\n",
    "\n",
    "rospy.sleep(5.0)\n",
    "\n",
    "stop = [0, 0, 0, 0]\n",
    "msg = Float32MultiArray(data=stop)\n",
    "pub.publish(msg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then execute the Python file with the following command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "$ cd ~/catkin_ws/src\n",
    "$ python basic_motions.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"bg-success text-center\">\n",
    "    - Exercise 3.1 -\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"images/holo_basic.gif\" width=\"200\" align=\"right\">\n",
    "\n",
    "In your workspace (**~/catkin_ws/src/**), create a new ROS package named **motion_mpo_500**. Inside this package, create a new Python script named **basic_motions_exercise.py**. Inside this script, add the necessary code for making the robot move with the rest of the basic motions (backward, left, right, rotations), as shown in the GIF aside. Program a sequence of all the motions (for 1 second each) and stop the robot at the end."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"bg-success text-center\">\n",
    "    - End of Exercise 3.1 -\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div>\n",
    "    <h3 class=\"text-center\">\n",
    "        <span class=\"text-primary\">3.3</span>\n",
    "        &nbsp;\n",
    "        <span class=\"\">Motion in the robot's frame</span>\n",
    "    </h3>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An omnidirectional robot can move in the plane and rotate simultaneously without restrictions. Given any planar twist $[\\omega_{bz}, v_{bx}, v_{by}]^T$ the corresponding speed of the wheels $u$ can be obtained with the kinematic model seen in 3.1."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"bg-success text-center\">\n",
    "    - Exercise 3.2 -\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the previous ROS package named **motion_mpo_500**, create a new Python script named **make_turn.py**. \n",
    "\n",
    "Inside the script, create a function named `twist2wheels`, which takes the elements of the planar twist as arguments and produces an array $u$ with the speed of the wheels. To do so, you must define the matrix of the kinematic model, and multiply this matrix and the twist vector.\n",
    "\n",
    "For the dimensions of the robot, please see the dimensions of the MPO-500 in the below figure (measures in mm). Additionally, the wheel diameter is 254mm.\n",
    "\n",
    "<img src=\"images/MPO-500.jpg\" align=\"center\">\n",
    "\n",
    "*Hint*: we recommend the use of the [NumPy module](https://numpy.org/) for matrix operations, specifically the [numpy.dot function](https://numpy.org/doc/stable/reference/generated/numpy.dot.html) for matrix/vector multiplication.\n",
    "\n",
    "The template for the function can be found in the cell below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def twist2wheels(wz, vx, vy):\n",
    "    #################\n",
    "    ### YOUR CODE ###\n",
    "    #################\n",
    "    return u"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"images/holo_twist.gif\" width=\"380\" align=\"right\">\n",
    "Add the following code to the script for testing the function (feel free to change the values of the twist). It computes the speed of the wheels for the given planar twist, and it sends the array of speeds to the robot controller. \n",
    "\n",
    "For the given values, the robot moves forward and simultaneously turns to the left, as shown in the figure.\n",
    "\n",
    "After a 1-second pause, the robot is stopped:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "u = twist2wheels(wz=1.5, vx=1, vy=0)\n",
    "msg = Float32MultiArray(data=u)\n",
    "pub.publish(msg)\n",
    "rospy.sleep(1)\n",
    "stop = [0,0,0,0]\n",
    "msg = Float32MultiArray(data=stop)\n",
    "pub.publish(msg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can reset the robot to its original position at any time by clicking on the *Reset the simulation* button\n",
    "\n",
    "<img src=\"images/reset_button.png\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"bg-success text-center\">\n",
    "    - End of Exercise 3.2 -\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div>\n",
    "    <h3 class=\"text-center\">\n",
    "        <span class=\"text-primary\">3.4</span>\n",
    "        &nbsp;\n",
    "        <span class=\"\">Motion in the absolute frame</span>\n",
    "    </h3>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the previous section, the twist is defined in the reference frame of the chassis. Sometimes it may be useful to define the velocity of the robot in the absolute frame.\n",
    "This can be easily done with the relationship between the chassis' planar twist $\\nu_b$ and its velocity $\\dot{q}$ in the space frame, defined in 3.1. \n",
    "\n",
    "There is a caveat, though: we **must know the orientation of the chassis $\\phi$ with respect to the space frame**.\n",
    "It can be measured by [odometry](https://en.wikipedia.org/wiki/Odometry), [inertial sensors](https://en.wikipedia.org/wiki/Inertial_measurement_unit), or an [external localization system](https://en.wikipedia.org/w/index.php?title=Robot_localization&redirect=no). \n",
    "\n",
    "We will use the odometry computed by the simulator.\n",
    "The following code subscribes to the `/odom` topic and continuously updates the value of the orientation of the robot in the global variable `phi`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from nav_msgs.msg import Odometry\n",
    "from tf.transformations import euler_from_quaternion\n",
    "\n",
    "def odom_callback(msg):\n",
    "    global phi   \n",
    "    position = msg.pose.pose.position\n",
    "    (_, _, phi) = euler_from_quaternion([msg.pose.pose.orientation.x, \n",
    "                                         msg.pose.pose.orientation.y, \n",
    "                                         msg.pose.pose.orientation.z, \n",
    "                                         msg.pose.pose.orientation.w])\n",
    "    \n",
    "position_sub = rospy.Subscriber(\"/odom\", Odometry, odom_callback)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"bg-success text-center\">\n",
    "    - Exercise 3.3 -\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the previous ROS package named **motion_mpo_500**, create a new Python script named **absolute_motion.py**. \n",
    "\n",
    "In this script, copy the `odom_callback` code (including the import statements) from the above cell, and create a function named `velocity2twist`, which takes the elements of the velocity in the space frame as arguments and computes the twist in the chassis' frame.\n",
    "\n",
    "*Hint*: recall section 3.1 for the relationship between these entities.\n",
    "\n",
    "The template for the function can be found in the cell below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def velocity2twist(dphi, dx, dy):\n",
    "    #################\n",
    "    ### YOUR CODE ###\n",
    "    #################\n",
    "    return wz, vx, vy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"images/holo_abs.gif\" align=\"right\" width=\"240\">\n",
    "\n",
    "Test the function with the following code (feel free to change the values of the velocity). \n",
    "\n",
    "For the given values, the robot moves foward on the X-axis direction of the global frame (the planar grid), and it turns simultaneously around its Z-axis, as seen in the figure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for _ in range(100):\n",
    "    wz, vx, vy = velocity2twist(dphi=1.5708, dx=1, dy=0)\n",
    "    u = twist2wheels(wz, vx, vy)\n",
    "    msg = Float32MultiArray(data=u)\n",
    "    pub.publish(msg)\n",
    "    rospy.sleep(0.01)\n",
    "    \n",
    "stop = [0,0,0,0]\n",
    "msg = Float32MultiArray(data=stop)\n",
    "pub.publish(msg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can reset the robot to its original position at any time with the following cell:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can reset the robot to its original position at any time by clicking on the *Reset the simulation* button\n",
    "\n",
    "<img src=\"images/reset_button.png\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"bg-success text-center\">\n",
    "    - End of Exercise 3.3 -\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"bg-success text-center\">\n",
    "    - Exercise 3.4 -\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"images/holo_square.gif\" align=\"right\" width=\"300\">\n",
    "\n",
    "In the previous ROS package named **motion_mpo_500**, create a new Python script named **square.py**. \n",
    "\n",
    "In this script, write the code for moving the robot in a 3m x 3m square trajectory, so that the robot turns 90º simultaneously during the translation along each edge of the square, as seen in the figure.\n",
    "\n",
    "The trajectory consists of four motions, each along one edge of the square. During each motion, the robot moves along the X- or Y-axis of the absolute frame, and simultaneously rotates around the Z-axis."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can reset the robot to its original position at any time with the following cell:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can reset the robot to its original position at any time by clicking on the *Reset the simulation* button\n",
    "\n",
    "<img src=\"images/reset_button.png\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"bg-success text-center\">\n",
    "    - End of Exercise 3.4 -\n",
    "</div>"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python [Root]",
   "language": "python",
   "name": "Python [Root]"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
